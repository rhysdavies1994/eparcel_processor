require 'csv'

class CsvUtils
  #Getters/Setters for class fields
  attr_accessor :data, :current_row_index, :current_col_index

  #Constructor
  def initialize(filepath)
    @data = CSV.read(filepath)
    # @csv.each do |row|
    #   p row
    # end

    @current_row_index=0
    @current_col_index=0
  end

  #Write current CSV file to output file
  def write(output_filename, mode = "w")
    CSV.open(output_filename, mode) do |csv|
      @data.each do |row|
        csv << row
      end
    end
  end

  #Append current CSV file to output file
  def append(output_filename)
    write(output_filename,"a")
  end

  #Return current row array
  def current_row
    return @data[@current_row_index]
  end

  #Return next row array
  def next_row
    return @data[@current_row_index+1]
  end

  #Return previous row array
  def previous_row
    return @data[@current_row_index-1]
  end

  #Return cell value of current row and column
  def current_col
    return @data[@current_row_index][@current_col_index]
  end

  #Return cell value of current row and next column
  def next_col
    return @data[@current_row_index][@current_col_index+1]
  end

  #Return cell value of current row and previous column
  def prev_col
    return @data[@current_row_index][@current_col_index-1]
  end

  #Return index of last row in csv file
  def last_row_index
    return @data.length
  end

  #Increase current row index by n
  def increment_row(n)
    @current_row_index += n
  end

  #Increase current column index by n
  def increment_col(n)
    @current_col_index += n
  end

  #Set specificied row in csv with an array
  def set_row(row_index,value)
    @data[row_index]=value
  end

  #Set specified cell in csv with a value
  def set_cell(row,column,value)
    @data[row][column] = value
  end

  #Check if current row is an eParcel consignment line, first cell is "C"
  def current_row_is_consignment_line?
    result = false
    result = true if current_row[0] == "C"
    return result
  end

  #Check if current row is an eParcel article line, first cell is "A"
  def current_row_is_article_line?
    result = false
    result = true if current_row[0] == "A"
    return result
  end

  #Return row data of next consignment line
  def next_consignment_line
    while current_row[0] != "C"
      @current_row_index += 1
    end

    return current_row
  end

  #Return row data of next article line
  def next_article_line
    while current_row[0] != "A"
      @current_row_index += 1
    end

    return current_row
  end

  #Return array of row data containing all article lines until next consignment
  def next_article_lines
    rows=[]

    if(current_row[0]=="A")
      rows.push(current_row)
    end

    while(next_row[0] == "A")
      @current_row_index += 1
      rows.push(current_row)
    end

    return rows
  end

  #Set current row and column index to 0
  def reset_counter
    @current_col_index=0
    @current_row_index=0
  end

end