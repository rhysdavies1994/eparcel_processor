# README #

TISC runs a large despatch of parcels a couple times a year. These are all run through eParcel and require a fair bit of data processing before the file can be uploaded to eParcel Online.

TISC has their own eParcel account, the username is “quickmail”. We have an account set up for them because it is such a large job, and this way we can get the postage charged directly to their account rather than putting more pressure on our accounts balance.

* The data processing is the same each year so I’ve actually written a script in ruby to automate the process as much as possible.
* This is available at https://bitbucket.org/rhysdavies1994/eparcel_processor/ and is shared with the Quickmail bitbucket account. Make sure this is downloaded or cloned somewhere.
* If you’re looking for the process of how TISC is processed, look at the older jobs and talk to Duncan.

# Steps to setting up TISC using my program:

**(if manually processing data without program, steps 3-5 are different)**

1 - Receive data file of all schools from TISC

2 - Receive books for distribution from Universities

* Find weight(g) of each book
* Find thickness(mm) of each book
* Amount of books per box
* Weight of each box
* Make sure weight*amount of books = weight of each box

3 - Open eparcel_processor program, inside main.rb change variables which represent books, boxes and input/output files.

4 - Open command prompt, go to eparcel_processor directory and run “ruby main.rb” 

* (install ruby for windows here if you don’t have it) 

5 - Open “eParcel Sorted Data” and delete all data in column C

6 - UPLOAD “eParcel Sorted Data” to eParcel online under TISCS account “quickmail”

7 - If any errors when uploading:

* Fix any errors inside of data file
* Go to “View Consignments”
* Delete all consignments
* Re-upload data file

8 - Once there are no errors, PRINT eParcel Labels

9 - Print “eParcel Manifest Data” on A3 landscape paper for contractors to pack off

10 - Print enough of the generic “letter to principal” for the amount of parcels required

11 -  Send out back for packing

12 - Once finished packing, go to eParcel online, Make sure all packages are assigned and click despatch