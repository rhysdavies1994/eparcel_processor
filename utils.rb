#Convert grams to kilogram formatted number
def to_kg(grams)
    result = (grams/1000.0).round(2)
    return result
end

