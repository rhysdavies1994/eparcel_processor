#include files and system libraries
require_relative 'e_parcel'
require_relative 'consignment_line'
require_relative 'article_line'
require_relative 'csv_utils'
require_relative 'box'
require_relative 'utils'
require_relative 'book'
require 'csv'

#Global variable, if true export manifest data to its own file
$create_manifest=true
$progress=0

#################################################
## CHANGE BELOW VARIABLES TO REPRESENT JOB ##
# REQUIRED: Declare Books to be used in eParcel(name, weight, thickness, quantity per box)
uwa = Book.new("UWA", 385, 8, 25) #UWA, weighs 340g, 8mm thick, 25 per box
notre_dame = Book.new("Notre Dame", 285, 6, 35) #Notre Dame, weighs 285g, 6mm thick, 35 per box
curtin = Book.new("Curtin University", 448, 8, 28) #Curtin University, weighs 448g, 8mm thick, 28 per box
ecu = Book.new("ECU", 340, 8, 48) #ECU, weighs 340g, 8mm thick, 48 per box
murdoch = Book.new("Murdoch University", 325, 5, 39) #Murdoch University, weighs 325g, 5mm thick, 39 per box
prospectuses = [uwa, notre_dame, curtin, ecu, murdoch]

# REQUIRED: Declare input and output data files
source_filename = './eparcel-data-2017.csv'
output_filename = 'eparcel Sorted Data.csv'
manifest_filename = 'eparcel Manifest Data.csv'

# REQUIRED: Declare boxes to be used(weight, height, length, width)
quickmailBox = Box.new(248+5, 210) #Quickmail packing box, weighs 248g + 5g for paper, is 210mm tall

################################################

#Begin Processing
puts "Reading from #{source_filename}"
csv = CsvUtils.new(source_filename) #Read in eparcel data file
output_file = CSV.open(output_filename, "w") #output data file for australia post eparcel format
manifest_file = CSV.open(manifest_filename, "w") if ($create_manifest) #output data file for australia post eparcel format

#variables for keeping track of current consignment
current_consignment_row=[]
amount_articles=0


def update_progress(percentage)
  $progress=percentage
  value = percentage.to_s + "%"
  value = "100% - Done!\n" if percentage == 100
  STDOUT.write "\rProcessing eParcel Data.. #{value}"
end

#Begin Processing eParcel Data
update_progress(0)
# print "#{ ((csv.current_row_index+1)/(csv.last_row_index+1)).round(2)}"
#Iterate through each row until the end
while (csv.current_row_index != csv.last_row_index)
  update_progress((((csv.current_row_index+1.0)/(csv.last_row_index+1.0))*100).round(2))
  #If Consignment Line
  if (csv.current_row_is_consignment_line?)
    consignment = ConsignmentLine.new(csv.current_row) #create consignment object for current row
    article = ArticleLine.new(csv.next_row) #create article object for first article, to reference quantity

    #Output updated consignment line
    output_file << consignment.to_row

    #Manifest output
    consignment.fields['C']="Quantity of each Book: "+article.fields['B'].to_i.to_s if ($create_manifest)
    manifest_file << consignment.to_row

    #Keep track of current consignment
    current_consignment_row=consignment.to_row
    amount_articles=0

    #Else if article line
  elsif (csv.current_row_is_article_line?) #If Article Line
    boxes=[] #Array to store boxes with articles inside

    #Process articles which fit into Complete Full Boxes
    prospectuses.each do |current_prospectus| #For each book

      #Create article line from current row and get amount of each book being sent
      article = ArticleLine.new(csv.current_row)
      amount_books = article.fields['B'].to_i

      #Calculate how many full boxes are needed for quantity of books
      amount_full_boxes = current_prospectus.amount_boxes_for(amount_books)

      #If atleast 1 full box
      if (amount_full_boxes > 0)

        #Store amount of box objects filled with books
        amount_full_boxes.times do
          box = Box.new
          box.create_full_box_of(current_prospectus)
          boxes << box
        end

        #Check if amount of articles are greater than 20 (maximum articles per consignment is 20)
        amount_articles +=amount_full_boxes
        if (amount_articles>=20) #If greater than max articles, split boxes to start new consignment
          remainder_full_boxes=amount_articles%20
          amount_full_boxes-=remainder_full_boxes
        end

        if amount_full_boxes > 0
          #Modify weight(B), number of identical articles(F)
          article.fields['B'] = to_kg(boxes.last.total_weight)
          article.fields['F'] = amount_full_boxes

          #Export articles to output file
          output_file << article.to_row

          #Manifest output
          article.fields['C'] = current_prospectus.name+"("+amount_full_boxes.to_s+" Boxes of "+current_prospectus.quantity_per_box.to_s+")" if ($create_manifest)
          manifest_file << article.to_row if ($create_manifest)
        end


        #If amount articles greater than max, create new consignment and add rest of articles not added above
        if (amount_articles>=20)
          output_file << current_consignment_row #Add current consignment to output file to extend it with new articles
          manifest_file << [] if ($create_manifest)
          manifest_file << current_consignment_row if ($create_manifest)

          amount_articles=amount_articles-20 #reset amount of articles

          if (amount_articles > 0)
            #Modify weight(B), number of identical articles(F)
            article.fields['B'] = to_kg(boxes.last.total_weight)
            article.fields['F'] = remainder_full_boxes

            #Output to file
            output_file << article.to_row

            #Manifest output
            article.fields['C'] = current_prospectus.name+"("+remainder_full_boxes.to_s+" Boxes of "+current_prospectus.quantity_per_box.to_s+")" if ($create_manifest)
            manifest_file << article.to_row if ($create_manifest)
          end
        end


      end
    end

    #Create initial box for storing first article
    box = Box.new

    #Process remainder of articles which aren't a complete box
    prospectuses.each do |current_prospectus|
      #Create article using current row and get amount of books needed
      article = ArticleLine.new(csv.current_row)
      amount_books = article.fields['B'].to_i

      #Calculate how many remainder books there are to be put into a box
      amount_remainder = current_prospectus.amount_individuals_for(amount_books)

      #If atleast 1 book to be allocated to a box
      if (amount_remainder > 0)

        #For amount of books needed
        amount_remainder.times do
          #Check if the book will fit in the box
          if (box.has_room_for(current_prospectus))
            box.add_book(current_prospectus.clone)
            #Else If there is no more room in box for the book
          else
            #Process current box to output file
            boxes<<box
            article.fields['B'] = to_kg(box.total_weight)
            article.fields['F'] = 1
            amount_articles +=1
            output_file << article.to_row

            #Manifest output
            article.fields['C'] = box.books_in_box if ($create_manifest)
            manifest_file << article.to_row if ($create_manifest)

            #If amount of articles is now 20 or more, duplicate consignment for future articles to same person
            if (amount_articles>= 20)
              amount_articles=amount_articles-20
              output_file << current_consignment_row
              manifest_file << [] if ($create_manifest)
              manifest_file << current_consignment_row if ($create_manifest)

            end

            #Create new box to store excess articles and add current book
            box = Box.new
            box.add_book(current_prospectus.clone)
          end
        end
      end
    end
    #Process last box and output to file
    boxes<<box
    article = ArticleLine.new(csv.current_row)
    article.fields['B'] = to_kg(box.total_weight)
    article.fields['F'] = 1
    amount_articles +=1
    output_file << article.to_row

    #Manifest output
    article.fields['C'] = box.books_in_box if ($create_manifest)
    manifest_file << article.to_row if ($create_manifest)

    #Space out consignments in manifest
    manifest_file << [] if ($create_manifest)

    #Else, Error in the eParcel format, only should be consignment or article lines
  else
    puts "Error: Unrecognised Line"
  end

  #Go to the next row
  csv.increment_row(1)
end

#If get here, output is completed
update_progress(100)


