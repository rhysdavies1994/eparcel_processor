class ConsignmentLine
  #Variable containing all column letters for consignments
  $columns = [*'A'..'BW']

  #Getter/Setter for csv fields
  attr_accessor :fields

  #Constructor which turns a row of data in a csv file into a Hashed array
  #There able to access csv fields by column letter e.g column 'AZ'
  def initialize(csv_row)
    @fields=Hash.new
    csv_row.each.with_index do |cell,index|
      @fields[$columns[index]] = cell
    end
  end

  #Turn fields hash into an array value for csv output
  def to_row
    result = []
    @fields.each_value do |field|
      result << field
    end
    return result
  end

end