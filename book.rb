require_relative 'box'

class Book
  #Getters/Setters for Class fields
  attr_accessor :name, :weight, :thickness, :quantity_per_box, :box_weight

  #Constructor
  def initialize(name="none",weight=1, thickness=1,quantity_per_box=1)
    @name=name                    #University name as a string
    @weight=weight                      #Weight of book as grams
    @thickness=thickness                #Thickness of book as millimetres
    @quantity_per_box=quantity_per_box  #Quantity per box as integer
  end

  #Calculate how many full boxes can be made from the quantity given
  def amount_boxes_for(quantity)
    result = (quantity / quantity_per_box).to_i
    return result
  end

  #Calculate how many books wont fit neatly into 1 box from the quantity given
  def amount_individuals_for(quantity)
    result = quantity % @quantity_per_box
    return result
  end
end