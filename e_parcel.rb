require_relative 'consignment_line'
require_relative 'article_line'

class EParcel
  attr_accessor :consignment_line, :article_lines

  def initialize(consignment_row, article_rows)
    @consignment_line = ConsignmentLine.new(consignment_row)
    @article_lines = []

    article_rows.each do |article_row|
      @article_lines.push(ArticleLine.new(article_row))
    end
  end

  def add_article_line(article_line)
    @article_lines.push(article_line)
  end

end