require_relative 'book'

class Box
  #Getters/Setters for Class fields
  attr_accessor :weight, :height, :length, :width, :books

  #constructor
  def initialize(weight=248,height=210, length=1, width=1)
    @weight=weight  #weight of empty box in grams
    @height=height  #height of box in mm
    @length=length  #length of box in mm
    @width=width    #width of box in mm
    @books=Array.new       #array of books currently in box
  end

  #Calculation to figure out how much space will be wasted in between books
  def space_taken_up_by_air
    return ((0.1*@height)+1).to_i
  end

  #Calculation to see how much space is left in box,
  def remaining_space
    result = @height
    @books.each do |book|
      result -= book.thickness
    end
    result = 0 if result<0
    return result
  end

  #Calculate to see total weight of box, all books plus box weight
  def total_weight
    result = @weight
    @books.each do |book|
      result += book.weight
    end
    return result
  end

  #Calculate how much space has been used by books
  def used_height
    result = 0
    @books.each do |book|
      result += book.thickness
    end
    return result
  end

  #Fill box with books of same time
  def create_full_box_of(book)
    book.quantity_per_box.times do
      @books.push(book)
    end

    @height=used_height
  end

  #Add a book to box
  def add_book(book)
    if(total_weight+book.weight < 22000 && remaining_space>=book.thickness)
      @books.push(book)
    else
      puts "No more room in box for book, currently #{@books.length} books"
    end
  end

  #Check if box has room for certain book
  def has_room_for(book)
    result=false
    thickness=0

    if(book.is_a?(Book))
      thickness=book.thickness
    end

    if(thickness <= ((remaining_space)-(space_taken_up_by_air)-thickness))
      result=true
    end

    return result
  end

  #Returns a string of different books and quantities in box
  def books_in_box
    result = ""
    bookHash = Hash.new(0)
    @books.each do |book|
      bookHash[book.name]+=1
    end

    index=1
    bookHash.each do |name,count|
      result += name+ " : " + count.to_s
      if(index != bookHash.length)
        result  += ", "
      end
      index+=1
    end

    return result
  end

  #Remove book at certain index of box
  def remove_book_at_index(index)
    @books.pop(index)
  end

  #Calculate how many books will fit with certain thickness
  def amount_books_with_thickness(thickness)
    result = @height/thickness
    result = (result*0.9).to_i - 1 #remove 10% + 1 to account for air between books
    return result
  end

end